#!/bin/sh

member='devops/conf/member'
sources='sources/'

while read -r line; do
  result="${sources}${line}.html"
  if [ ! -f $result ]; then
    echo ${result}" not found"
    exit 1
  fi
done < $member