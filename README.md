# CI/CD

![CI/CD](sources/cicd.svg)

La CI/CD (intégration continue/livraison continue) est un processus qui vise à automatiser la construction, les tests et le déploiement de logiciels.
L'objectif principal de la CI/CD est de permetttre aux équipes de développement de livrer de nouvelles fonctionnalités de manière rapide et fiable, tout en minimisant les risques liés au déploiements.

Le processus de CI/CD commence par l'intégration continue, qui consiste à intégrer de nouvelles modifications de code dans le logiciel de manière régulière, Cela permet de détecter rapidement les erreurs et les bogues, ce qui facilite leur correction avant que le logiciel ne soit mis en production.

La livraison continue, quant à elle, implique le déploiement automatisé de nouvelles versions du logiciel dès qu'elles sont prêtes à être mises en production. 
Cela permet aux équipes de développement de mettre en production de nouvelles fonctionnalités de manière rapide et de réagir rapidement aux demandes des utilisateurs.

En résumé, la CI/CD vise à accélérer le processus de développement de logiciels tout en garantissant une qualité élevée et en minimisant les risques liés au déploiement.

# Qu'est-ce que la CI ?

L'intégration continue (CI) est une pratique de développement de logiciels qui consiste à intégrer de nouvelles modifications de code dans le logiciel de manière régulière, généralement plusieurs fois par jour. Cela permet de détecter rapidement les erreurs et les bogues, ce qui facilite leur correction avant que le logiciel ne soit mis en production.

Le processus d'intégration continue comprend généralement plusieurs étapes, comme la compilation du code, l'exécution de tests automatisés et l'analyse statique du code. Si toutes ces étapes sont réussies, le logiciel est considéré comme prêt à être déployé en production.

# Qu'est-ce que la CD ?

La livraison continue (CD) est une pratique de développement de logiciels qui consiste à déployer automatiquement les nouvelles versions du logiciel dès qu'elles sont prêtes à être mises en production. Le processus de livraison continue implique généralement l'automatisation de tâches telles que le déploiement, le test et la mise à disposition des nouvelles versions du logiciel aux utilisateurs finaux.

La livraison continue vise à accélérer le processus de mise en production de nouvelles fonctionnalités en automatisant les tâches fastidieuses et en permettant aux équipes de développement de se concentrer sur l'ajout de nouvelles fonctionnalités et sur la résolution des problèmes.

## Plan

L'étape "plan" de la CI/CD fait référence à la planification des nouvelles versions du logiciel et des modifications apportées au code source. Cette étape comprend généralement les tâches suivantes :

1. Élaboration de la stratégie de développement : il est important de déterminer comment les modifications apportées au code seront intégrées et déployées en production.
2. Élaboration de la roadmap de développement : il est important de déterminer les fonctionnalités à ajouter et les objectifs à atteindre dans les versions futures du logiciel.
3. Élaboration des spécifications techniques : il est important de déterminer comment les modifications apportées au code seront implémentées techniquement et de documenter ces décisions.
4. Gestion du code source : il est important de gérer le code source de manière organisée et contrôlée, en utilisant des outils de gestion de version tels que Git.

En résumé, l'étape "plan" de la CI/CD concerne la planification des nouvelles versions du logiciel et des modifications apportées au code source. Cela comprend l'élaboration de la stratégie de développement, de la roadmap de développement et des spécifications techniques, ainsi que la gestion du code source.

## Code

L'étape "code" de la CI/CD fait référence au développement et à la modification du code source du logiciel. Cette étape comprend généralement les tâches suivantes :

1. Écriture de code : il s'agit de créer de nouvelles fonctionnalités ou de modifier le code existant en suivant les spécifications techniques établies à l'étape "plan".
2. Test du code : il est important de tester le code pour s'assurer qu'il fonctionne correctement et qu'il répond aux exigences.
3. Soumission de code : une fois le code terminé et testé, il peut être soumis pour intégration dans le code source principal du logiciel.

En résumé, l'étape "code" de la CI/CD concerne le développement et la modification du code source du logiciel, y compris l'écriture de code, le test du code et la soumission de code pour intégration.

## Build

L'étape "build" de la CI/CD fait référence à la compilation et à l'assemblage du code source en vue de créer une version exécutable du logiciel. Cette étape comprend généralement les tâches suivantes :

1. Compilation du code : il s'agit de transformer le code source en un fichier exécutable en utilisant un compilateur ou un interpréteur.
2. Assemblage des dépendances : le logiciel peut dépendre de bibliothèques ou de frameworks externes, qui doivent être inclus dans le build final.
3. Création d'un package : le build final peut être empaqueté sous forme de fichier exécutable, de paquet d'installation ou d'image de conteneur, selon les besoins.

En résumé, l'étape "build" de la CI/CD concerne la compilation et l'assemblage du code source en vue de créer une version exécutable du logiciel, y compris la compilation du code, l'assemblage des dépendances et la création d'un package.

## Test

L'étape "test" de la CI/CD fait référence aux tests effectués sur le logiciel afin de s'assurer qu'il fonctionne correctement et répond aux exigences. Cette étape comprend généralement les tâches suivantes :

1. Élaboration de cas de test : il est important de déterminer quelles parties du logiciel doivent être testées et de créer des cas de test pour couvrir ces parties.
2. Exécution de tests automatisés : il est recommandé d'exécuter des tests automatisés pour vérifier le bon fonctionnement du logiciel de manière répétitive et rapide.
3. Tests manuels : il peut être nécessaire de réaliser des tests manuels pour vérifier le bon fonctionnement du logiciel et pour couvrir les cas de test qui ne peuvent pas être automatisés.

En résumé, l'étape "test" de la CI/CD concerne les tests effectués sur le logiciel afin de s'assurer qu'il fonctionne correctement et répond aux exigences, y compris l'élaboration de cas de test, l'exécution de tests automatisés et les tests manuels.

## Release et Deploy

L'étape "release/deploy" de la CI/CD fait référence au déploiement de la nouvelle version du logiciel en production. Cette étape comprend généralement les tâches suivantes :

1. Préparation du déploiement : il est important de s'assurer que tous les éléments nécessaires au déploiement sont prêts, tels que les fichiers exécutables et les scripts de déploiement.
2. Déploiement en production : le logiciel est déployé en production, généralement en utilisant des outils de déploiement automatisés.
3. Vérification du déploiement : il est important de vérifier que le logiciel fonctionne correctement en production et de résoudre tout problème éventuel.

En résumé, l'étape "release/deploy" de la CI/CD concerne le déploiement de la nouvelle version du logiciel en production, y compris la préparation du déploiement, le déploiement en production et la vérification du déploiement.

Il est important de noter que l'étape "deploy" est souvent utilisée de manière interchangeable avec l'étape "release" dans le contexte de la CI/CD. Toutefois, certaines personnes utilisent "release" pour se référer à l'ensemble du processus de déploiement, y compris la préparation et la vérification, alors que "deploy" se réfère spécifiquement au déploiement en production.

## Operate

L'étape "operate" de la CI/CD fait référence à l'opération et à la maintenance du logiciel une fois qu'il a été déployé en production. Cela inclut les tâches suivantes :

1. Surveillance du logiciel : il est important de surveiller le logiciel en production pour s'assurer qu'il fonctionne correctement et pour détecter tout problème éventuel.
2. Résolution de problèmes : lorsqu'un problème est détecté, il est important de le résoudre rapidement pour minimiser l'impact sur les utilisateurs.
3. Mise à jour du logiciel : il peut être nécessaire de mettre à jour le logiciel en production avec de nouvelles versions ou des correctifs.
4. Gestion des changements : il est important de gérer les changements effectués sur le logiciel en production de manière organisée et contrôlée pour minimiser les risques liés aux mises à jour.

En résumé, l'étape "operate" de la CI/CD concerne l'opération et la maintenance du logiciel en production pour s'assurer qu'il fonctionne correctement et pour résoudre tout problème éventuel.

## Monitor

L'étape "monitor" de la CI/CD fait référence à la surveillance du logiciel une fois qu'il a été déployé en production. Cette étape comprend généralement les tâches suivantes :

1. Surveillance du fonctionnement du logiciel : il est important de surveiller le fonctionnement du logiciel en production pour s'assurer qu'il fonctionne correctement et pour détecter tout problème éventuel.
2. Collecte de données de performance : il peut être utile de collecter des données de performance, telles que les temps de réponse et la utilisation des ressources, afin de surveiller l'efficacité du logiciel et de détecter tout problème de performance.
3. Alerte en cas de problème : il est important de mettre en place des alertes pour détecter rapidement tout problème éventuel et pour permettre aux équipes de développement de réagir rapidement.

En résumé, l'étape "monitor" de la CI/CD concerne la surveillance du logiciel en production pour s'assurer qu'il fonctionne correctement et pour détecter tout problème éventuel, y compris la collecte de données de performance et l'alerte en cas de problème.